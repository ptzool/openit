variable "access_key" {}
variable "secret_key" {}

provider "aws" {
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "eu-west-1"
}

resource "aws_instance" "openit-webserver" {
    ami = "ami-099f067e"
    instance_type = "t1.micro"
    availability_zone = "eu-west-1a"
    security_groups = ["default"]
    tags = {
        Env = "prod"
        Role = "webserver"
        Project = "openit"
        Name = "openit-webserver"
    }
    key_name = "zool"
    count = 2
}
resource "aws_elb" "openit-elb" {
    name = "openit-elb"
    availability_zones = ["eu-west-1a"]

        listener = {
        instance_port = 80
        instance_protocol = "http"
        lb_port = 80
        lb_protocol = "http"
    }

    health_check {
        healthy_threshold = 2
        unhealthy_threshold = 2
        timeout = 3
        target = "HTTP:80/index.html"
        interval = 10
    }
    instances = ["${aws_instance.openit-webserver.*.id}"]
    cross_zone_load_balancing = true
}


output "ip" {
    value = "${aws_elb.openit-elb.dns_name}"
}
